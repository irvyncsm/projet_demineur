import java.util.*;

public class Demineur {
    private boolean gameOver = false;
    private int score = 0;

    public Demineur(int nbLignes, int nbColonnes, int pourcentage){
    }

    public int getScore(){
        return this.score;
    }

    public void reveler(int x, int y){
    }
    
    public void marquer(int x, int y){
        
    }

    public boolean estGagnee(){
        return this.gameOver==true;
    }

    public boolean estPerdu(){
        return this.gameOver==false;
    }

    public void reset(){
    }

    public void affiche(Plateau plat){
    }

    public void nouvellePartie(){
    }

    
}
