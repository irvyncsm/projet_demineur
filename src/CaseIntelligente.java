import java.util.*;

public class CaseIntelligente extends Case{
    private List<Case> lesVoisines;

    public CaseIntelligente(){}
    
    public void ajouteVoisine(Case uneCase){
    }

    public int nombreBombesVoisines(){
        return 0;
    }

    @Override
    public String toString(){
        if (!this.estDecouverte()){
            if (this.estMarquee()){
                return "?";
            }
            return " ";
        }
        if (this.contientUneBombe()){
            return "@";
        }
        return String.valueOf(nombreBombesVoisines());
    }

}
