import java.util.*;

public class Plateau {
    private int nbLignes;
    private int nbColonnes;
    private int pourcentageDeBombes;
    private int nbBombes;
    private List<List<CaseIntelligente>> lesCases;

    public Plateau(int nbBombes, int nbLignes, int nbColonnes){
        this.nbBombes=nbBombes;
        this.nbLignes=nbLignes;
        this.nbColonnes=nbColonnes;
        this.lesCases= new ArrayList<>();
        // for (int i=0; i<this.nbLignes; i++){
        //     List<Case> listeCases = new ArrayList<>();
        //     for (int j=0; j<this.nbColonnes; j++){
        //         listeCases.add(new Case(false, false, false));
        //     }
        //     this.lesCases.add(listeCases);
        // }
    }

    private void creerLesCasesVides(){
    }

    private void rendLesCasesIntelligentes(){
    }

    protected void poseDesBombesAleatoirement(){
    }

    public int getNbLignes(){
        return this.nbLignes;
    }

    public int getNbColonnes(){
        return this.nbColonnes;
    }

    public int getNbTotalBombes(){
        return this.nbBombes;
    }

    public CaseIntelligente getCase(int numLignes, int numColonne){
        return null;
    }

    public int getNbCases(){
        return this.nbLignes*this.nbColonnes;
    }

    public int getNbCasesMarquee(){
        return 0;
    }

    public void poserBombe(int x, int y){
    }

    public void reset(){
    }

}
