public class Case {
    private boolean contientUneBombe = false;
    private boolean estDecouverte = false;
    private boolean estMarquee = false;

    public Case(){}

    public void reset(){
        this.contientUneBombe=false;
        this.estDecouverte=false;
        this.estMarquee=false;
    }

    public void poseBombe(){
        this.contientUneBombe=true;
    }

    public boolean contientUneBombe(){
        return this.contientUneBombe;
    }

    public boolean estDecouverte(){
        return this.estDecouverte;
    }

    public boolean estMarquee(){
        return this.estMarquee;
    }

    public boolean reveler(){
        return this.estDecouverte=true;
    }

    public boolean marquer(){
        return this.estMarquee=true;
    }

    // public boolean setCachee(){
    //     return this.estDecouverte=false;
    // }


    // public boolean deleteBombe(){
    //     return this.contientUneBombe=false;
    // }

    // public boolean deleteMarquee(){
    //     return this.estMarquee=false;
    // }
    
}
